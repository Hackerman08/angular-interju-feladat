import { Component, OnInit } from '@angular/core';
import { User } from '@model/user';
import { UserService } from '@service/user.service';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

    public users: User[];



    constructor(private userService: UserService) { }

    ngOnInit() {
        this.reload();
    }

    reload() {
        this.userService.getUsers().subscribe(res => this.users = res);
    }

    onSelect(user: User): void {
    this.selectedHero = user;
  }
}
